﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    /*Instance é usado para não precisar dar new na classe ou getComponent
     quando for querer usar em outro arquivo*/
    public static GameController _instance;

    /*Vai ficar todos os 
    estados referente ao funcionamento geral(multijogo) */
    public enum State
    {
        Menu,
        Playing,
        Settings
    }

    public State state;

    void Awake()
    {
        if (_instance == null)
            _instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        state = State.Menu;
    }


    void Update()
    {
        switch (state)
        {
            case State.Menu:
                LoadThisScene("Menu");
                break;
            case State.Playing:
                LoadThisScene("Playing");
                break;
            case State.Settings:
                LoadThisScene("Settings");
                break;
        }
    }

    public void ChangeState(State _state)
    {
        state = _state;
    }

    public void LoadThisScene(string name)
    {
        if (SceneManager.GetActiveScene().name != name)
            SceneManager.LoadScene(name);
    }
}
